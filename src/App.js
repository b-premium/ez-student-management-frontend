import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import './App.css';
import BootstrapTable from "./Components/Tables/BootstrapTable";
import axios from "axios";
import SlideOutLeftNavContent from "./Components/Menu/SlideOutLeftNavContent";
import MenuBar from "./Components/MenuBar";
 export const BACKEND_URL =
  "https://ez-student-management-api.herokuapp.com/api/v1/students";

class App extends Component {
  state = {
    isMenuOpen: false,
    students: []
  };

  updateStudentTableData(data) {
    this.setState({
      students: data
    });
  }

  getStudents = () => {
    axios
      .get(
        BACKEND_URL,
        {},
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            Accept: "*/*"
          }
        }
      )
      .then(json => {
        this.updateStudentTableData(json.data);
      })
      .catch(err => {
        console.log("------Error.");
      });
  };

  addStudentToState = student => {
    this.setState(prevState => ({
      students: [...prevState.students, student]
    }));
  };

  updateState = student => {
    const studentIndex = this.state.students.findIndex(
      data => data.id === student.id
    );
    const newArray = [
      ...this.state.students.slice(0, studentIndex),
      student,
      ...this.state.students.slice(studentIndex + 1)
    ];
    this.setState({ students: newArray });
  };

  deleteStudentFromState = id => {
    const updatedStudents = this.state.students.filter(
      student => student.id !== id
    );
    this.setState({ students: updatedStudents });
  };

  componentDidMount() {
    this.getStudents();
  }

  openSideNav = () => {
    this.setState({
      isMenuOpen: true
    });
  };

  closeSideNav = () => {
    this.setState({
      isMenuOpen: false
    });
  }

  filterFrontEndData(studentsData,inputString, field, secondInputString, secondField){
    let outPutArray = [];
    studentsData.filter(function (data) {
      console.log(`------data=${JSON.stringify(data)}`);
      const currentField =  data[field];
      console.log(`currentField=${currentField}`);
      const currentFieldTwo =  data[secondField];
         if(currentField.includes(inputString)){
           return outPutArray.push(data);
         } else if(currentFieldTwo.includes(secondInputString)){
           console.log(`data=${data}`);
           return outPutArray.push(data);

         }
    });
    return outPutArray;
  }

  localSearchByFirstNameAndOrLastName = (firstName, lastName) => {
    console.log(`firstName=${firstName}  lastName=${lastName}`);
    const studentsData = this.state.students;
    const filteredData = this.filterFrontEndData(studentsData,firstName, 'firstName', lastName, 'lastName');
    console.log(`-----filteredData=${JSON.stringify(filteredData)}`);
    this.updateStudentTableData(filteredData);
  };

  render() {
    return (
      <div>
        <SlideOutLeftNavContent isMenuOpen={this.state.isMenuOpen} closeSideNav={this.closeSideNav} />
        <Container className="App">
          <Row>
            <Col>
              <MenuBar openSideNav={this.openSideNav}
                       addStudentToState={this.addStudentToState}
                       localSearchByFirstNameAndOrLastName={this.localSearchByFirstNameAndOrLastName} />
            </Col>
          </Row>
          <Row>
            <Col>
              <BootstrapTable
                students={this.state.students}
                updateState={this.updateState}
                deleteStudentFromState={this.deleteStudentFromState}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
