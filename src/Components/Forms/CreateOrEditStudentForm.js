import React from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import {BACKEND_URL} from "../../App";

class CreateOrEditStudentForm extends React.Component {
  state = {
    id: 0,
    firstName: "",
    lastName: "",
    gpa: ""
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  submitAddNewStudentForm = e => {
    e.preventDefault();
    fetch(BACKEND_URL, {
      method: "post",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        gpa: this.state.gpa
      })
    })
      .then(response => response.json())
      .then(student => {
        if (student.id > 0) {
          this.props.addStudentToState(student);
          this.props.toggle();
        } else {
          console.log("failure");
        }
      })
      .catch(err => console.log(err));
  };

  submitEditNewStudentForm = e => {
    e.preventDefault();
    fetch(`${BACKEND_URL}/${this.state.id}`, {
      method: "put",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: this.state.id,
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        gpa: this.state.gpa
      })
    })
      .then(response => response.json())
      .then(student => {
        if (student.id > 0) {
          this.props.updateState(student);
          this.props.toggle();
        } else {
          console.log("failure");
        }
      })
      .catch(err => console.log(err));
  };

  componentDidMount() {
    if (this.props.student) {
      const { id, firstName, lastName, gpa } = this.props.student;
      this.setState({ id, firstName, lastName, gpa });
    }
  }

  render() {
    return (
      <Form
        onSubmit={this.props.student ? this.submitEditNewStudentForm : this.submitAddNewStudentForm}
      >
        <FormGroup>
          <Label for="firstName">First Name</Label>
          <Input
            type="text"
            name="firstName"
            id="firstName"
            onChange={this.onChange}
            value={this.state.firstName === null ? "" : this.state.firstName}
          />
        </FormGroup>
        <FormGroup>
          <Label for="lastName">Last Name</Label>
          <Input
            type="text"
            name="lastName"
            id="lastName"
            onChange={this.onChange}
            value={this.state.lastName === null ? "" : this.state.lastName}
          />
        </FormGroup>
        <FormGroup>
          <Label for="gpa">GPA</Label>
          <Input
            type="gpa"
            name="gpa"
            id="gpa"
            onChange={this.onChange}
            value={this.state.gpa === null ? "" : this.state.gpa}
          />
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    );
  }
}

export default CreateOrEditStudentForm;
