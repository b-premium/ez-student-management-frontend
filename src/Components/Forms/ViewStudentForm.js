import React from "react";
import {Form, FormGroup, Label } from "reactstrap";
import PropTypes from 'prop-types';

const ViewStudentForm = ({student}) => {
    const {firstName, lastName, gpa } = student;
    return (
      <Form>
        <FormGroup>
          <Label for="firstName" className="ezmf-modal-field-title">First Name:</Label>
          <Label id="firstName">{firstName}</Label>
        </FormGroup>
        <FormGroup>
          <Label for="lastName" className="ezmf-modal-field-title">Last Name: </Label>
          <Label id="lastName">{lastName}</Label>
        </FormGroup>
        <FormGroup>
          <Label for="gpa" className="ezmf-modal-field-title">GPA: </Label>
          <Label id="gpa">{gpa}</Label>
        </FormGroup>
      </Form>
    );
};

ViewStudentForm.propTypes = {
    student: PropTypes.object
};

export default ViewStudentForm;
