import React, {Component} from 'react';

class SlideOutLeftNavButton extends Component {
    render() {
        return (
            <span className="esm-menu" onClick={this.props.openSideNav}>&#9776; EzStudentMng</span>
        );
    }
}

export default SlideOutLeftNavButton;