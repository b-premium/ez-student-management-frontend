import React, {Component} from 'react';

class SlideOutLeftNavContent extends Component {

    aboutUsClicked = () => {
        alert('To do: implement About Us Page');
    };

    parentClicked = () => {
        alert('To do: implement Parent Page');
    };

    studentClicked = () => {
        alert('To do: implement Student Page.');
    };

    contactUsClicked = () => {
        alert('To do: implement Contact us Page.');
    };

    render() {
        const {isMenuOpen, closeSideNav} = this.props;
        if(isMenuOpen){
            return (
                <div id="mySidenav" className="esm-sidenav">
                    <span className="closeButton" onClick={closeSideNav}>&times;</span>
                    <span className="navButton" onClick={this.aboutUsClicked}>About Us</span>
                    <span className="navButton" onClick={this.studentClicked}>Student</span>
                    <span className="navButton" onClick={this.parentClicked}>Parent</span>
                    <span className="navButton" onClick={this.contactUsClicked}>Contact Us</span>
                </div>
            )
        }
        return <div />
    }
}

export default SlideOutLeftNavContent;