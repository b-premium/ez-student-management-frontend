import React, { useState } from 'react';
import {
    InputGroup,
    InputGroupAddon,
    InputGroupButtonDropdown,
    Input,
    Button,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import SlideOutLeftNavButton from "../../Menu/SlideOutLeftNavButton";
import ModalForm from "../../Modals/ModalForm";

const MenuBarDemo = (props) => {
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [splitButtonOpen, setSplitButtonOpen] = useState(false);
    const [firstName, setFirstNameValue] = useState('');
    const [lastName, setLastNameValue] = useState('');


    const toggleDropDown = () => setDropdownOpen(!dropdownOpen);

    const toggleSplit = () => setSplitButtonOpen(!splitButtonOpen);

    return (
        <div>
            <InputGroup>
                <InputGroupAddon addonType="prepend"><Button>I'm a button</Button></InputGroupAddon>
                <Input />
            </InputGroup>
            <br />
            <InputGroup>
                <Input />
                <InputGroupButtonDropdown addonType="append" isOpen={dropdownOpen} toggle={toggleDropDown}>
                    <DropdownToggle caret>
                        Button Dropdown
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem header>Header</DropdownItem>
                        <DropdownItem disabled>Action</DropdownItem>
                        <DropdownItem>Another Action</DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>Another Action</DropdownItem>
                    </DropdownMenu>
                </InputGroupButtonDropdown>
            </InputGroup>
            <br />
            <InputGroup>
                <InputGroupAddon addonType="prepend">
                    <Button outline>
                        <SlideOutLeftNavButton openSideNav={props.openSideNav} />
                    </Button>
                    <ModalForm
                        buttonLabel="Add Student"
                        addStudentToState={props.addStudentToState}
                    />
                </InputGroupAddon>
                <Input placeholder="First Name" />
                <Input placeholder="Last Name" />
                <InputGroupAddon addonType="append">
                    <Button color="primary" onClick={()=> props.searchByFirstNameAndOrLastName()}>Search</Button>
                </InputGroupAddon>
            </InputGroup>
        </div>
    );
};

export default MenuBarDemo;