import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Input, InputGroup, InputGroupAddon} from "reactstrap";
import SlideOutLeftNavButton from "../Menu/SlideOutLeftNavButton";
import ModalForm from "../Modals/ModalForm";

class MenuBar extends Component {
    state = {
        firstName: '',
        lastName: ''
    };

    onFirstNameChange = (e) => {
        this.setState({
            firstName: e.target.value
        })
    };

    onLastNameChange = (e) => {
        this.setState({
            firstName: e.target.value
        })
    };

    searchByFirstNameAndOrLastName = () => {
        const {firstName, lastName} = this.state;
        this.props.localSearchByFirstNameAndOrLastName(firstName, lastName)
    }

    render() {
        const {openSideNav, addStudentToState} = this.props;
        return (
            <InputGroup>
                <InputGroupAddon addonType="prepend">
                    <Button outline>
                        <SlideOutLeftNavButton openSideNav={openSideNav} />
                    </Button>
                    <ModalForm
                        buttonLabel="Add Student"
                        addStudentToState={addStudentToState}
                    />
                </InputGroupAddon>
                <Input placeholder="First Name" onChange={this.onFirstNameChange} />
                <Input placeholder="Last Name" onChange={this.onLastNameChange} />
                <InputGroupAddon addonType="append">
                    <Button color="primary" onClick={this.searchByFirstNameAndOrLastName}>Search</Button>
                </InputGroupAddon>
            </InputGroup>
        );
    }
}

MenuBar.propTypes = {
  openSideNav: PropTypes.func.isRequired,
  addStudentToState: PropTypes.func.isRequired,
  localSearchByFirstNameAndOrLastName: PropTypes.func.isRequired
};

export default MenuBar;