import React, { Component } from "react";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import CreateOrEditStudentForm from "../Forms/CreateOrEditStudentForm";
import ViewStudentForm from "../Forms/ViewStudentForm";
import PropTypes from 'prop-types';

class ModalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };
  }

  toggle = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  };

  closeButton = (toggle) => (
      <button className="close" onClick={toggle}>
        &times;
      </button>
  );

  modalTitle(label){
    if (label === "Edit") {
      return  "Edit Student";
    } else if (label === "View") {
      return  "Viewing Student";
    }
    return  "Add New Student";
  }

  modalButton(label){
    if (label === "Edit") {
      return (
          <Button
              color="warning"
              onClick={this.toggle}
              className="esmf-ModalFormDeleteButton"
          >
            {label}
          </Button>
      );
    } else if (label === "View") {
      return (
          <Button
              color="info"
              onClick={this.toggle}
              className="esmf-ModalFormDeleteButton"
          >
            {label}
          </Button>
      );
    } else {
      return (
          <Button
              color="success"
              onClick={this.toggle}
              className="esmf-ModalFormEditButton"
          >
            {label}
          </Button>
      );
    }
  }

  modalBody = (label) => {
    if (label === "View") {
      return (
          <ViewStudentForm student={this.props.student} />
      )
    } else {
      return (
        <CreateOrEditStudentForm
          addStudentToState={this.props.addStudentToState}
          updateState={this.props.updateState}
          toggle={this.toggle}
          student={this.props.student}
        />
      )
    }
  }

  render() {
    const closeBtn =this.closeButton(this.toggle);
    const label = this.props.buttonLabel;
    let button = this.modalButton(label);
    let title = this.modalTitle(label);
    let modalBody = this.modalBody(label);

    return (
      <div>
        {button}
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle} close={closeBtn}>
            {title}
          </ModalHeader>
          <ModalBody>
            {modalBody}
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

ModalForm.propTypes = {
  student: PropTypes.any,
  className: PropTypes.string,
  addStudentToState: PropTypes.func,
  updateState: PropTypes.func
};

export default ModalForm;
