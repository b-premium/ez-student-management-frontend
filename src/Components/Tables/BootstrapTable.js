import React, { Component } from "react";
import { Table } from "reactstrap";
import TableBody from "./TableBody";
import {BACKEND_URL} from "../../App";
import PropTypes from 'prop-types';

class BootstrapTable extends Component {
  deleteStudent = id => {
    let confirmDelete = window.confirm(
      `Are you sure you want to permanently delete student with id: ${id}`
    );
    if (confirmDelete) {
      fetch(`${BACKEND_URL}/${id}`, {
        method: "delete",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          id
        })
      })
        .then(response => response.json())
        .then(student => {
          this.props.deleteStudentFromState(id);
          this.props.toggle();
        })
        .catch(err => console.log(err));
    }
  };

  render() {
    const { students, updateState } = this.props;

    return (
      <Table striped responsive hover bordered>
        <thead>
          <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>GPA</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <TableBody
            students={students}
            deleteStudent={this.deleteStudent}
            updateState={updateState}
          />
        </tbody>
      </Table>
    );
  }
}

BootstrapTable.propTypes = {
  students: PropTypes.array.isRequired,
  updateState: PropTypes.func.isRequired
};

export default BootstrapTable;
