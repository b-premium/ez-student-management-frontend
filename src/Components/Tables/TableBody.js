import React from "react";
import ModalForm from "../Modals/ModalForm";
import { Button } from "reactstrap";
import PropTypes from 'prop-types';

const TableBody = ({ students, deleteStudent, updateState }) =>
  students.map(student => {
    return (
      <tr key={student.id}>
        <th scope="row">{student.id}</th>
        <td>{student.firstName}</td>
        <td>{student.lastName}</td>
        <td>{student.gpa}</td>
        <td>
          <div className="actionButtonWidth">
            <ModalForm
              buttonLabel="View"
              student={student}
              updateState={updateState}
            />{" "}
            <ModalForm
              buttonLabel="Edit"
              student={student}
              updateState={updateState}
            />{" "}
            <Button color="danger" onClick={() => deleteStudent(student.id)}>
              Del
            </Button>
          </div>
        </td>
      </tr>
    );
  });

TableBody.propTypes = {
  students: PropTypes.array.isRequired,
  deleteStudent: PropTypes.func.isRequired,
  updateState: PropTypes.func.isRequired
};

export default TableBody;
